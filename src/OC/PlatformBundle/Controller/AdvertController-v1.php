<?php

namespace OC\PlatformBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdvertController extends Controller
{
  public function indexAction() {
    //$content = $this->get('templating')->render('OCPlatformBundle:Advert:index.html.twig');
    //  return new Response($content);

    //  return $this->render('OCPlatformBundle:Advert:index.html.twig');

    // relative url
    //$url = $this->generateUrl('oc_platform_view', ['id' => 5]);
    $url = $this->get('router')->generate(
           'oc_platform_view', // 1er argument : le nom de la route
           array('id' => 5)    // 2e argument : les valeurs des paramètres
     );

     // absolute url
    //$url = $this->get('router')->generate('oc_platform_home', array(), UrlGeneratorInterface::ABSOLUTE_URL);

       return new Response("L'URL de l'annonce d'id 5 est : ".$url);
  }

  public function viewAction($id, Request $request)
  {

    // On récupère notre paramètre tag
     $tag = $request->query->get('tag');

     return $this->render('OCPlatformBundle:Advert:view.html.twig', array(
       'id'  => $id,
       'tag' => $tag,
     ));

      return $this->redirectToRoute('oc_platform_home');
      return new JsonResponse(array('id' => $id));

      // Récupération de la session
      $session = $request->getSession();

      // On récupère le contenu de la variable user_id
      $userId = $session->get('user_id');

      // On définit une nouvelle valeur pour cette variable user_id
      $session->set('user_id', 91);

      // On n'oublie pas de renvoyer une réponse
      return new Response("<body>Je suis une page de test, je n'ai rien à dire</body>");
  }

  public function addAction(Request $request) {
    $session = $request->getSession();

    // Bien sûr, cette méthode devra réellement ajouter l'annonce

    // Mais faisons comme si c'était le cas
    $session->getFlashBag()->add('info', 'Annonce bien enregistrée');

    // Le « flashBag » est ce qui contient les messages flash dans la session
    // Il peut bien sûr contenir plusieurs messages :
    $session->getFlashBag()->add('info', 'Oui oui, elle est bien enregistrée !');

    // Puis on redirige vers la page de visualisation de cette annonce
    return $this->redirectToRoute('oc_platform_view', array('id' => 5));
  }

  public function viewSlugAction($slug, $year, $_format)
   {
       return new Response(
           "On pourrait afficher l'annonce correspondant au
           slug '".$slug."', créée en ".$year." et au format ".$_format."."
       );
   }
}
